#  Task 1
#  Зформуйте строку, яка містить певну інформацію про символ в відомому слові.
# Наприклад "The [номер символу] symbol in [тут слово] is '[символ з відповідним порядковим номером]'".
# Слово та номер отримайте за допомогою input() або скористайтеся константою.
# Наприклад (слово - "Python" а номер символу 3) -
# "The 3 symbol in "Python" is 't' ".

symbol = "Python"
index = symbol[symbol.index('o')]
print(f"The {symbol.index('o')+1} symbol in 'Python' is {index}")

# # Precondition your word should contain 'a' / просто вклав щоб не загубити
# word = input("Enter your word:")
# idx = 0
# tmp_str = ''
#
# while idx <= len(word) - 1:
#     if word[idx].lower() == 'a':
#         tmp_str += f'{idx}, '
#     idx += 1
#
# print(f'The {tmp_str} in "{word}" is "a"')

# #Написати цикл, який буде вимагати від користувача ввести слово, в якому є буква "о" (враховуються як великі так і маленькі).
# # Цикл не повинен завершитися, якщо користувач ввів слово без букви о.

# while True:
#     user_input = input("Enter some word: ")
#     if user_input.isalnum() and "O" in user_input:
#         print("Word contains upper O")
#         break
#     elif user_input.isalnum() and "o" in user_input:
#         print("Word contains lower o")
#         break
#     else:
#         print("Try again")

while True
    word = input("Word:")
    word = word.lower()
    if "o" in word:
        print("O is present")
    else:
        print("Try again")


print("End of Task 2")

