# Напишіть декоратор, який перетворює результат роботи функції на стрінг
# Напишіть докстрінг для цього декоратора

def my_list_decorator(func):
    """
    Function for converting function result to str
    :return: str
    """
    def wrapper(*args, **kwargs):
        res = func(*args, **kwargs)
        str_list = "".join([str(i) for i in my_list])
        print(str_list)
        return res
    return wrapper
@my_list_decorator
def add_element_in_list(list2):

    list2.append(9)
    return list2

my_list = [1, 2, 1.3, 8]
my_list = add_element_in_list(my_list)
