# написати до кожної функції мінімум 5 assert
# 1. напишіть функцію, яка перевірить, чи передане їй число є парним чи непарним (повертає True False)
def check_int_value(number):
    if number % 2 == 0:
        return True
    else:
        return False

assert type(check_int_value(8)) == bool
assert check_int_value(8) is True
assert check_int_value(9) is False
assert check_int_value(2.0) is True
assert check_int_value(3.3) is False

# 2. напишіть функцію. яка приймає стрічку, і визначає, чи дана стрічка відповідає результатам роботи методу capitalize()
# (перший символ є верхнім регістром, а решта – нижнім.) (повертає True False)
def if_str_is_capitalize(string):

    if string == string.capitalize():
        return True
    else:
        return False
assert type(if_str_is_capitalize("")) == bool
assert if_str_is_capitalize("my God") is False
assert if_str_is_capitalize("Hello world!") is True
assert type(if_str_is_capitalize("My name")) != str
assert if_str_is_capitalize("GOOOD") is False

# написати декоратор, який добавляє принт з результатом роботи отриманої функції
# + текстовий параметр, отриманий ним (декоратор з параметром - це там, де три функції)
# при цьому очікувані результати роботи функції не змінюються (декоратор просто добавляє принт)

def add_print_to_func(text):
    def decorator(func):
        def wrapper(*args, **kwargs):
            result = func(*args, **kwargs)
            print(text, result)

            return result
        return wrapper
    return decorator
@add_print_to_func("New text")
def add_element_in_list(list2):

    list2.append(9)
    return list2

my_list = [1, 2, 1.3, 8]
my_list = add_element_in_list(my_list)

