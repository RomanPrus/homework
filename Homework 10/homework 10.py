import datetime
import uuid
from art import *


class Bank_Account:
    __bank_money = 0

    def __init__(self, name, balance, percentage):
        """ Initialize self"""
        self.name = name
        self.balance = balance
        self.__percentage = percentage
        self.__date = datetime.datetime.today()
        self.__id = uuid.uuid4()
        Bank_Account.accounting(balance, income=True)

    @classmethod
    def accounting(cls, balance: int, /, *, income: bool):
        if income:
            Bank_Account.__bank_money += balance
        else:
            Bank_Account.__bank_money -= balance

    def __str__(self):
        return f'{self.name}, {self.balance}, {self.__percentage},{self.__date}'

    def add_to_balance(self, amount):
        """ Function to add some amount into balance"""
        self.balance += amount
        Bank_Account.accounting(self.balance, income=True)
        return self.balance

    def withdraw_from_the_balance(self, amount):
        """ Function to withdraw some amount from the balance"""
        if amount <= self.balance:
            self.balance -= amount
            Bank_Account.accounting(self.balance, income=False)

    def transfer_money(self, other, amount):
        """Function for transferring money from one account to another"""
        if other.balance >= amount:
            self.balance += amount
            other.balance -= amount
        else:
            self.balance += other.balance
            other.balance = 0

    @property
    def acc_percentage(self):
        return self.__percentage

    @acc_percentage.setter
    def acc_percentage(self, percent: int):
        """
        Function to change percentage for balance
        Expected any int value"""
        self.__percentage = percent
        return self.__percentage

    def get_today_profit(self):
        """Function to daily account profit according to the account percentage"""
        today_profit = (self.__percentage / 365) * self.balance
        return round(today_profit)

    @staticmethod
    def bank_slogan():
        """ This function print Bank slogan"""
        tprint("Give us your money and we will give you a candy after deposit will be end", font="cybermedium")

    def __del__(self):
        Bank_Account.accounting(self.balance, income=False)
        print("Your account was closed due to Bank liquidation.", self.balance, "was withdraw from account.", self.name,
              "doesn't have any claims to the Bank")
        self.balance -= self.balance


acc1 = Bank_Account('Alex', 5000, 10)
acc1.acc_percentage = 8
acc1.add_to_balance(500)
acc1.withdraw_from_the_balance(600)

acc2 = Bank_Account('Poroh', 10000, 10)
acc2.transfer_money(acc1, 800)
Bank_Account.bank_slogan()
