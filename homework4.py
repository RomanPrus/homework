# task 1
#Є list з даними lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
# Напишіть код, який свормує новий list (наприклад lst2), який містить лише змінні типу стрінг, які присутні в lst1.
# Зауважте, що lst1 не є статичним і може формуватися динамічно.


list1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
list2 = []
print(list1)
list1.append('False')
for element in list1:
    if type(element) == str:
        list2.append(element)
print(list2)
print("Task one is finish")

#Task 2
#Є list довільних чисел, наприклад [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44].
# Напишіть код, який видалить з нього всі числа, які менше 21 і більше 74.
list1 = [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44]
list2 = list1.copy()
print(list1)
for num in list1:
    if num < 21:
        list1.remove(num)
for num in list1:
    if num > 74:
        list1.remove(num)
print(list2)
print(list1)
print("end task 2")

#task3
#Є стрінг з певним текстом (можна скористатися input або константою).
# Напишіть код, який визначить кількість слів в цьому тексті, які закінчуються на 'о'.

test_str = "Hello my name so logo 40"
letter = 'o'
res = 0
x = test_str.split()
for i in x:
    if (i.endswith(letter) != -1):
        res += 1
print("Words count : " + str(res))
print("end task 3")


